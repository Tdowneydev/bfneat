package pairwise.idiom.neat

import kotlin.math.absoluteValue

/**
 *
 * main() will test sizes for a hidden node count.  for big counts, use a fixed heap -Xms and -Xmx together
 *
 * Pop:  POP: 1 HIDDEN: 1 (8582555640, 8589934592) <-  2048k to 0.0k ea@ 2048Kb
Pop:  POP: 1 HIDDEN: 10 (8582555640, 8589934592) <-  2048k to 0.0k ea@ 2048Kb
Pop:  POP: 1 HIDDEN: 50 (8582555640, 8589934592) <-  2048k to 0.0k ea@ 2048Kb
Pop:  POP: 1 HIDDEN: 100 (8582555640, 8589934592) <-  2048k to 0.0k ea@ 2048Kb
Pop:  POP: 1 HIDDEN: 200 (8582555640, 8589934592) <-  2048k to 0.0k ea@ 2048Kb
Pop:  POP: 1 HIDDEN: 500 (8580458488, 8589934592) <-  4096k to 0.0k ea@ 4096Kb
Pop:  POP: 1 HIDDEN: 1000 (8576264184, 8589934592) <-  8192k to 0.0k ea@ 8192Kb
Pop:  POP: 1 HIDDEN: 2000 (8551098360, 8589934592) <-  32768k to 0.0k ea@ 32768Kb
Pop:  POP: 1 HIDDEN: 5000 (8376994728, 8589934592) <-  202791k to 0.0k ea@ 202791Kb
Pop:  POP: 1 HIDDEN: 7000 (8179846440, 8589934592) <-  395318k to 0.0k ea@ 395318Kb
Pop:  POP: 1 HIDDEN: 9000 (7917154968, 8589934592) <-  651853k to 0.0k ea@ 651853Kb
Pop:  POP: 1 HIDDEN: 10000 (7760392040, 8589934592) <-  804942k to 0.0k ea@ 804942Kb
Pop:  POP: 1 HIDDEN: 12000 (7395072328, 8589934592) <-  1161699k to 0.0k ea@ 1161699Kb
Pop:  POP: 1 HIDDEN: 15000 (6732919592, 8589934592) <-  1808333k to 0.0k ea@ 1808333Kb
Pop:  POP: 1 HIDDEN: 20000 (5264209352, 8589934592) <-  3242620k to 0.0k ea@ 3242620Kb
Pop:  POP: 1 HIDDEN: 25000 (3341398776, 8589934592) <-  5120365k to 0.0k ea@ 5120365Kb
Pop:  POP: 1 HIDDEN: 30000 (1178283040, 8589934592) <-  7232782k to 0.0k ea@ 7232782Kb

 */
internal class JeanTest{

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val runtime = Runtime.getRuntime()
            val env = object : Environment {}
            val baseMax = runtime.maxMemory()


            repeat (100)
            {
                arrayOf(1,
                    1,
                    1,
                    1,
                    1,
                    1,
                    1,
                    1,
                    10,
                    10,
                    10,
                    10,
                    10,
                    10,
                    10,
                    10,
                    10,
                    10,
                    50,
                    50,
                    50,
                    50,
                    50,
                    50,
                    100,
                    200,
                    500,
                    1000,
                    2000,
                    5000,
                    7000,
                    9000,
                    10000,
                    12000,
                    15000,
                    20000,
                    25000,
                    30000).distinct()/*.reversed()*/.onEach { HCOUNT ->
                    System.gc()
                    Thread.sleep(100)
                    val baseFree = runtime.freeMemory()
                    val baseTotal = runtime.totalMemory()
                    Environment.sim = Sim(
                        INPUTS = 0,
                        BIAS = 0,
                        HIDDEN = HCOUNT,
                        OUTPUTS = 0,
                        POPULATION = 1,
                        CONN_DENSTIY = 0.00001,
                    )

                    val population = Population(Array<Gnome>(Environment.sim.POPULATION) { Gnome() }.toList())
                    val rb = (runtime.freeMemory() - baseFree.toDouble()).absoluteValue.toLong()
                    val rk = rb.absoluteValue / 1024
                    val tk = (runtime.totalMemory() - baseTotal.toDouble()).absoluteValue / 1024
                    println("Pop:  POP: ${Environment.sim.POPULATION} HIDDEN: ${Environment.sim.HIDDEN} ${runtime.freeMemory() to runtime.totalMemory()} <-  ${rk}k to ${tk}k ea@ ${rb / Environment.sim.POPULATION / 1024}Kb")

                }
                System.gc()
                Thread.sleep(10000)
            }


        }
    }
}
