package pairwise.idiom.neat

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.take
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import pairwise.idiom.neat.Environment.Companion.sim
import vec.util._l
import kotlin.math.PI
import kotlin.math.absoluteValue
import kotlin.random.Random

class Xor {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            runBlocking {
                ActivationFunction.cached -= listOf(ActivationFunction.Power)
                sim = Sim(
                    INPUTS = 2,
                    BIAS = 0,
                    HIDDEN = 17,
                    OUTPUTS = 1,
                    POPULATION = 80,
                    CONN_DENSTIY = .3
                )
                val population = Population(List(sim.POPULATION) { Gnome() })
                while (true) evaluate(population)
            }
        }


        val elite = mutableSetOf<Gnome>()
        var generation = -0
        var ladder = .7
        var fidelity = 1
        fun evaluate(population: Population) {
            runBlocking(Dispatchers.Default) {
                for (gnome in population.gnomes)/* run*/ launch {
                    val raw = (0 until fidelity).map {
                        val jeans = gnome.jeans
                        for (k in sim.hiddenRange.start..sim.outputRange.endInclusive) {
                            val jean = jeans[k]
                            if (!jean.gated) jean.impulse = .0
                        }
                        val src = BooleanArray(sim.INPUTS) { Random.nextBoolean() }
                        src.toDoubleArray().forEachIndexed { index, d -> jeans[index].impulse = d }
                        val truth = booleanArrayOf(src[0].xor(src[1])).toDoubleArray()
                        for ((key, jean) in jeans.withIndex()) {
                            jean.inputs?.run {
                                val links = jean.links
                                for (x in links) {
                                    assert(x >= 0) {
                                        "found bad value"
                                    }
                                    val jean1 = gnome.jeans[x]
                                    if (key != x && jean1.gated || key > x) {
                                        val wt = jean.weights?.get(x)
                                        var v = jean1.af(jean1.impulse)
                                        if (!v.isFinite() || v == Double.NEGATIVE_INFINITY) {
                                            GnoMutations.sundown.op(gnome)
                                            v = Random.nextDouble(-PI, PI)
                                            /*System.err.println("** sundown **")*/
                                        }

                                        assert(v.isFinite()) {
                                            "Fail with ${jean1.af}, ${
                                                jean1.links.withIndex().filter { (_, target) ->
                                                    jeans[target].gated || target >= x
                                                }.map { (_, t) -> jeans[t].af }.toList()
                                            }"
                                        }
                                        (v * (wt ?: 0.0)).also { jean.impulse += it }
                                    }
                                }
                            }
                        }
                        val outputs = sim.outputRange.map { jeans[it] }.map { it.impulse }
                        truth.withIndex().map { (i, d) ->
                            1.0 - (outputs[i].minus(d)).absoluteValue
                        }.average()

                    }
                    gnome.points = raw.average().also {
                        if (it < -1e9) {
                            GnoMutations.sundown.op(gnome)/*
                                System.err.println("** sundown **")*/
                        }
                    }
                }
            }
            var standings: MutableList<Gnome> =
                population.gnomes.sortedByDescending { it.points }.toMutableList()


            val topscore = standings.first().points
            val champion = standings.first()
            val top = champion.points
            if (topscore > ladder) {
                if ((topscore) > .9999) {
                    ladder = 0.7
                    fidelity++

                    if (elite.size > 10) elite.remove(elite.random())
                    elite.add(champion)
                    System.err.println(elite.indexOf(champion) to champion)
                } else ladder = topscore


                standings.remove(champion)
                if (top > .9999) {
                    System.err.println("elite")
                    System.err.println(champion)
                }
            }

            val scores = standings.map { it.points }

            System.err.println("fidelity: $fidelity generation: $generation top: ${top.toFloat()}/${ladder.toFloat()} mean:${scores[scores.size / 2]} avg: ${
                scores.average().toFloat()
            } low: ${scores.minOrNull()} pop: ${standings.size}")
            if (top.isNaN()) {
                population.gnomes.withIndex().filter { (_, gnome) -> gnome.points.isNaN() }.forEach {
                    System.err.println(it.toString())
                }
                throw  Error("NaN happened")
            }

            generation++
            standings = standings.dropLast((standings.size.toDouble() / 2.86543).toInt()).toMutableList()
            population.gnomes = emptyList()

            if (elite.isNotEmpty()) standings.add(Gnome(elite.random()))
            runBlocking {
                //mutate the bottom half
                standings.takeLast(standings.size / 2).forEach { GnoMutations.chances.random().op(it) }
                population.gnomes = flow<Gnome> {
                    (_l[
                            Gnome(champion),
                            GnoMutations.theClones,
                            Gnome(champion).apply(GnoMutations.pureEvil.op::invoke),

                    ].also { it.onEach { emit(it) } } +
                            //breed the top half
                            standings).take(standings.size / 2).zipWithNext { a, b ->
                        emit(a.procreate(b).also { GnoMutations.chances.random().op(it) })
                    }
                    while (true) emit(Gnome())
                }.take(Environment.sim.POPULATION).toList(ArrayList(Environment.sim.POPULATION))
            }
        }
    }
}
