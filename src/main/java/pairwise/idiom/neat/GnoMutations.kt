package pairwise.idiom.neat

import vec.macros.`➤`
import vec.macros.get
import vec.macros.toVect0r
import java.util.*
import kotlin.math.PI
import kotlin.random.Random

enum class GnoMutations(val fraction: Int, val op: (Gnome) -> Unit) {
    hotspot(1, { g ->
        val hotspot = Environment.sim.hiddenRange.random()
        val hs = g.jeans[hotspot]
        Environment.sim.inputRange.forEach { hs.inputs!!.set(it) }
        Environment.sim.outputRange.forEach { g.jeans[it].inputs!!.set(hotspot) }
    }),

    Yline(4, { g ->
        val iside = Environment.sim.inputRange.random()
        Environment.sim.hiddenRange.forEach { i: Int -> g.jeans[i].inputs!!.set(iside) }
        g.jeans[Environment.sim.outputRange.random()].inputs!!.set(Environment.sim.hiddenRange.random())

    }),

    boostbias(Environment.sim.BIAS * 5, { g: Gnome ->
        val value = g.jeans[Environment.sim.hiddenRange.random()]
        val random = Environment.sim.biasRange.random()
        value.inputs!!.set(random)
        value.weights!![random] = 1.0
    }),

    jeanMutation6pack(50, { g ->
        repeat(6) {
            JeanMutations.chances.random().op(g.jeans[Environment.sim.hiddenRange.random()])
        }
    }),

    jeanMutation12pack(10, {

            g ->
        repeat(12) {
            JeanMutations.chances.random()
                .op(g.jeans[Environment.sim.hiddenRange.random()])
        }
    }),
    sundown(3, { g ->
        g.jeans.forEach { j ->
            j.weights?.indices?.forEach { x: Int ->
                j.weights!![x] /= 2.0
            }
        }
    }),
    rattle(17, { g ->
        g.jeans.forEach { j ->
            j.weights?.indices?.forEach { x: Int ->
                j.weights!![x] *= Random.nextDouble(-1.1, 1.1)
            }
        }
    }),
    quake(1, { g ->
        g.jeans.forEach { j ->
            j.weights?.indices?.forEach { x: Int ->
                j.weights!![x] *= Random.nextDouble(-PI, PI)
            }
        }
    }),
    pureEvil(0, { g ->

        g.jeans.map { j ->
            Jean(
                gated = !j.gated,
                af = j.af,
                impulse = -j.impulse,
                inputs = (j.inputs?.clone() as BitSet?)?.apply {
                    (0..Environment.sim.hiddenRange.last).forEach(this::flip)
                },
                weights = j.weights?.map(ActivationFunction.NegatedLinear::invoke)?.toDoubleArray()
            )
        }
    }),

    /**
     * utility to create minimal flat 3-layer revolvable/shuffleable tumblers
     */
    Diagonal(0, { g ->
        g.jeans.onEach { it.inputs?.clear() }
        Environment.sim.apply {
            var hiddeniterator = hiddenRange.iterator()
            var inputiterator = inputRange.iterator()
            var outputIterator = outputRange.iterator()
            val terminator = vec.util._a[0, 0, 0]
            while (true) {
                if (!inputiterator.hasNext()) inputiterator = inputRange.iterator().also { terminator[0]++ }
                if (!hiddeniterator.hasNext()) hiddeniterator = hiddenRange.iterator().also { terminator[1]++ }
                if (!outputIterator.hasNext()) outputIterator = outputRange.iterator().also { terminator[2]++ }
                if ((0) !in terminator) break
                val inp = inputiterator.nextInt()
                val hidden = hiddeniterator.nextInt()
                val outp = outputIterator.nextInt()
                g.jeans[hidden].inputs!!.set(inp)
                g.jeans[outp].inputs!!.set(hidden)
            }
        }
    }
    )
    ;

    companion object {
        /**
         * clone wrs reference
         */
        val jangoFett = (Gnome().apply { Diagonal.op(this) })
        val theClones
            get() = Gnome(jangoFett).apply {
                jeans = (
                        jeans.take(Environment.sim.hiddenRange.first) +
                                jeans.toVect0r()[Environment.sim.hiddenRange].`➤`.shuffled()
                                    .also { it.onEach { it.weights!!.shuffle() } } +
                                jeans.toVect0r()[Environment.sim.outputRange].`➤`.shuffled()
                                    .also { it.onEach { it.weights!!.shuffle() } }).toTypedArray()

            }

        val cached = values()

        /**
         * call chances.random() for a mutation
         */
        val chances = sequence {
            for (gnoMutations in cached) {
                repeat(gnoMutations.fraction) {
                    yield(gnoMutations)
                }
            }
        }.toList()
    }
}
