package pairwise.idiom.neat

import kotlin.random.Random

enum class JeanMutations(val fraction: Int, val op: (Jean) -> Unit) {
    flipGate(2, { it.gated = it.gated.xor(it.gated) }),
    activateWeight(5,
        {
            it.weights?.run {
                this[Random.nextInt(0, size)] = ActivationFunction.cached.random().invoke(this[Random.nextInt(0, size)])
            }
        }),
    perturbWeight(5, {
        it.weights?.run {
            ActivationFunction.EvSailSigmoid.invoke(this[Random.nextInt(0, size)])
                .also { this[Random.nextInt(0, size)] = it }
        }
    }),
    flipConnectionBits(fraction = 25,
        { jean ->
            jean.inputs?.run {
                repeat(Random.nextInt(0,
                    5)) { jean.inputs!!.flip(Random.nextInt(jean.inputs!!.size())) }
            }
        }),
    ;

    companion object {

        val cached = values()

        /**
         * call chances.random() for a mutation
         */
        val chances = sequence {
            for (jeanMutations in cached) {
                repeat(jeanMutations.fraction) {
                    yield(jeanMutations)
                }
            }
        }.toList()

    }
}
