package pairwise.idiom.neat

data class Sim(
    val INPUTS: Int,
    val BIAS: Int,
    val HIDDEN: Int,
    val OUTPUTS: Int,
    val POPULATION: Int,
    val CONN_DENSTIY: Double,
) {
    val inputRange = 0 until INPUTS
    val biasRange = (inputRange.last + 1).let { start -> start until (start + BIAS) }
    val hiddenRange = (biasRange.last + 1).let { start -> start until (start + HIDDEN) }
    val outputRange = (hiddenRange.last + 1).let { start -> start until (start + OUTPUTS) }
}
