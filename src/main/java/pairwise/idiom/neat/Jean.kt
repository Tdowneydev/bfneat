package pairwise.idiom.neat

import java.util.*
import kotlin.random.Random

class Jean(
    var gated: Boolean = Random.nextInt(0, 9) == 0,
    var af: ActivationFunction = ActivationFunction.Linear,
    var impulse: Double = 1.0,
    var inputs: BitSet? = BitSet(Environment.sim.hiddenRange.endInclusive).also { bs ->
        repeat((Environment.sim.CONN_DENSTIY * Environment.sim.outputRange.first).toInt()) {
            bs.set(Random.nextInt(0, Environment.sim.outputRange.first))
        }
    },
    var weights: DoubleArray? = DoubleArray(Environment.sim.outputRange.first) { x -> Random.nextDouble() },
) {


    val links: Sequence<Int>
        get() = sequence {
            inputs?.let { var x = 0; while ((inputs!!.nextSetBit(x).also { x = it }) in 0 ..  Environment.sim.hiddenRange.last) {
   assert(x!=-1){
       "bad value"
   }
                yield(x++)
            }
            }
        }

    constructor(cloned: Jean) : this(cloned.gated,
        cloned.af,
        cloned.impulse,
        cloned.inputs?.clone() as BitSet?,
        cloned.weights?.clone())

    override fun toString(): String {
        return "Jean(gated=$gated, af=$af, impulse=$impulse, inputs=${links.toList()}, weights=${weights?.contentToString()})"
    }

    companion object {
    }

}
