@file:Suppress("NOTHING_TO_INLINE")

package pairwise.idiom.neat


import pairwise.idiom.neat.ActivationFunction.Companion.cached
import pairwise.idiom.neat.Environment.Companion.sim
import kotlin.math.absoluteValue
import kotlin.random.Random

class Gnome(
    var jeans: Array<Jean> = Array<Jean>(sim.outputRange.last+1) { x ->
        when {
            x in sim.inputRange -> {
                Jean(weights = null, gated = false, inputs = null)
            }
            x in sim.biasRange -> {
                Jean(weights = null, gated = false, impulse = 1.0, inputs = null)
            }
            x in sim.hiddenRange -> {
                Jean(af = cached.random()/*[x % cached.size]*/)
            }
            x in sim.outputRange -> {
                Jean(gated = false)
            }
            else -> TODO()
        }
    },
) {
    constructor(parent: Gnome) : this(Array(sim.outputRange.last+1) { Jean(parent.jeans[it]) })

    var points: Double = Double.NEGATIVE_INFINITY
    val complexity get() = jeans.map { it.inputs?.cardinality() ?: 0 }.sum()


    fun procreate(mate: Gnome): Gnome {
        val jeans1 = Array(jeans.size) { i -> Jean(point(this, mate).jeans[i]) }
        jeans1.indices.forEach { jeans1[it].af = point(this, mate).jeans[it].af }
        return Gnome(jeans1 )
    }

    @OptIn(ExperimentalStdlibApi::class)
    override fun toString(): String {//specialization to tweak the links output since jean doesn't hold its own index point in toString
        val tree = sortedSetOf<Int>()
        val search = DeepRecursiveFunction<Int, Unit> {
            if (tree.add(it)) (usefulScope(it) intersect sim.hiddenRange).onEach { callRecursive(it) }
        }
        sim.outputRange.toList().onEach(search::invoke)
        return "Gnome(points=$points, jeans=${
            tree.map { index ->
                val jean = jeans[index]
                when (index) {
                    in sim.inputRange ->
                        index to "INPUT"
                    in sim.biasRange -> index to "BIAS"
                    else -> {
                        val visibleLinks = sim.inputRange + sim.biasRange + tree.intersect(usefulScope(index))
                        index to "Jean(gated=${jean.gated}, " +
                                "af=${jean.af}, " +
                                "impulse=${jean.impulse}, " +
                                "inputs=${visibleLinks.takeUnless { it.isEmpty() } ?: "-"}, " +
                                "weights=${
                                    jean.weights?.filterIndexed { index, _ -> index in visibleLinks }
                                        ?.map { it.toFloat().toString().take(4) }
                                })"
                    }
                }
            }
        }}"
    }

    fun usefulScope(index: Int) = jeans[index].links.filter {
        jeans[it].gated && index == it || it < index
    }.toList()

    companion object {
        inline fun point(g1: Gnome, g2: Gnome): Gnome =
            if ((Random.nextDouble(g1.points.absoluteValue + g2.points.absoluteValue)) < g1.points) g1 else g2
    }
}
