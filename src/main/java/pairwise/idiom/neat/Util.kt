package pairwise.idiom.neat


fun BooleanArray.toDoubleArray() = DoubleArray(size, ::toDouble as (Int) -> Double)
fun BooleanArray.toDouble(it: Int): Double = this[it].toDouble()
fun Boolean.toDouble(): Double = if (this) 1.0 else .0
